"""Vision operations.

Uses CV2 for edge detection
Uses pytesseract for OCR
"""

import math
import os

from PIL import Image, ImageDraw, ImageGrab, ImageOps
import numpy
import cv2
import pytesseract
from tabulate import tabulate

from .region import Region


class VisionOps:
    def __init__(self, screen_size: list, debug_mode: bool = False):
        self._debug_mode = debug_mode
        self._screen_size = screen_size

        # Configure tesseract
        cmd = os.environ.get('TESSERACT_CMD', r'/usr/bin/tesseract')
        pytesseract.pytesseract.tesseract_cmd = cmd

    def find_text(self, im: Image, line1_only: bool = False) -> dict:
        """Runs OCR on input image.


        @input im: PIL image
        @return: dict<string, (x,y,w,h)>
        """
        data = pytesseract.image_to_data(im)
        data_rows = data.split('\n')
        if self._debug_mode:
            # print(rows[3])
            table = list()
            table.append(data_rows[0].split())
            for row in data_rows[1:]:
                cols = row.split()
                if len(cols) < 12:
                    continue
                # print(cols, len(cols))
                table.append(cols)
            print(tabulate(table))

        d = dict()
        for row in data_rows[1:]:
            cols = row.split()
            if len(cols) < 12:
                continue
            if line1_only and cols[4] != '1':
                break

            label = cols[-1]
            xywh_string = cols[-6:-2]
            xywh = [int(s) for s in xywh_string]
            d[label] = tuple(xywh)
        return d

    def locate_mainwindow(self, est_screen_rect: list, padding: int = 100) -> tuple:
        """Determine screen position of window using its title bar.

        @input est_screen_rect: list [x, y, w, h]
        @return delta x,y from estimated screen position to measured position
        """
        # Grab image at estimated center of titlebar +/- N pixels
        if self._debug_mode:
            print('EST_SCREEN_RECT', est_screen_rect)
        x, y, w, h = est_screen_rect
        x_center = x + w // 2
        y_center = y - 20  # using wrong offset because y is way off
        x1 = max(0, x_center - padding)
        y1 = max(0, y_center - padding)
        x2 = min(self._screen_size[0], x_center + padding)
        y2 = min(self._screen_size[1], y_center + padding)

        im_bbox = (x1, y1, x2, y2)
        if self._debug_mode:
            print('IM_BBOX', im_bbox)
        color_img = ImageGrab.grab(bbox=im_bbox)
        gray_img = color_img.convert('L')
        im = gray_img

        # Find horizontal edges
        cv_image = numpy.array(im)
        edges = cv2.Canny(cv_image, 80, 120)
        lines = cv2.HoughLinesP(edges, 1, math.pi/2, 2, None, 30, 1)
        if self._debug_mode:
            print('lines', lines.shape)
        # print()

        # Reshape to 2D reduced
        reduced_shape = (lines.shape[0], lines.shape[2])
        reduced = numpy.reshape(lines, reduced_shape)

        # Sort by y1 coordinate
        sorted = reduced[numpy.argsort(reduced[:,1])]
        if self._debug_mode:
            print('lines', sorted)

        y_estimate = est_screen_rect[1] - y1
        y_measured = sorted[0][1]
        y_delta = y_measured - y_estimate

        if self._debug_mode:
            print('LOCATE Y estimate', y_estimate)
            print('LOCATE Y measured', y_measured)
            print('LOCATE Y delta   ', y_delta)

            # TODO save ylocate.png with edges highlighted
            # pt1 = sorted[0][0], sorted[0][1]
            # pt2 = pt1[0] + sorted[0][2], pt1[1] + sorted[0][3]
            # cv2.rectangle(im, pt1, pt2, 255, -1)
            filename = 'locate_y.png'
            im.save(filename)
            print('Wrote', filename)

        # To locate in x, grab image of title bar (middle 50%)
        x, y, w, h = est_screen_rect
        x1 = x + w // 4
        y1 = est_screen_rect[1] + y_delta
        x2 = x + w - w // 4
        y2 = y1 + 40 # should be dead nuts

        if self._debug_mode:
            im_bbox = (x1, y1, x2, y2)
            print('IM_BBOX', im_bbox)
            color_img = ImageGrab.grab(bbox=im_bbox)
            gray_img = color_img.convert('L')
            im = ImageOps.invert(gray_img)
            filename = 'locate_x.png'
            im.save(filename)
            print('Wrote', filename)

        x_min = x2
        x_max = x1
        d = self.find_text(im, line1_only=True)
        for rect in d.values():
            x_left = x1 + rect[0]
            x_right = x_left + rect[2]
            # print('XLEFT, XRIGHT', x_left, x_right)

            x_min = min(x_min, x_left)
            x_max = max(x_max, x_right)
        x_center = (x_min + x_max) // 2
        if self._debug_mode:
            print('x_min, x_max, XCENTER', x_min, x_max, x_center)

        x_estimate = est_screen_rect[0] + est_screen_rect[2] // 2
        x_measured = (x_min + x_max) // 2
        x_delta = x_measured - x_estimate

        if self._debug_mode:
            print('LOCATE X estimate', x_estimate)
            print('LOCATE X measured', x_measured)
            print('LOCATE X delta   ', x_delta)

        return (x_delta, y_delta)
