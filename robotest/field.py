"""Field region.

A region which must be located at runtime.
"""

from .region import Region, RegionType

class Field(Region):
    """Geometric region without a predefined location; is located at runtime.

    """
    def __init__(self, name, position=[0, 0], size=[None, None]):
        super(Field, self).__init__(name, position, size)
        self.action_offset = [0, 0]

    def type(self):
        return RegionType.Field

    def screen_point(self, include_action_offset = False):
        """Returns centroid coordinates."""
        coords = super(Field, self).screen_point()
        if include_action_offset:
            x = coords[0] + self.action_offset[0]
            y = coords[1] + self.action_offset[1]
            coords = (x, y)
        return coords
