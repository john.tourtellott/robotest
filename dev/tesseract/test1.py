"""Adapted from https://github.com/madmaze/pytesseract"""

import argparse
import os
import sys

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# If you don't have tesseract executable in your PATH, include the following:
pytesseract.pytesseract.tesseract_cmd = r'/usr/bin/tesseract'
# Example tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'

parser = argparse.ArgumentParser(description='Run tesseract on input image')
parser.add_argument('-i', '--image_file', required=False, help="Input image")
parser.add_argument('-t', '--threshold', type=int, required=False, help="Apply gray threshold, between 1 and 254")
args = parser.parse_args()

if args.image_file is not None:
    gray_im = Image.open(args.image_file)
    if args.threshold is not None:
        tess_im = gray_im.point(lambda p: p > args.threshold and 255)
    else:
        tess_im = gray_im
    print(pytesseract.image_to_data(tess_im))
    tess_im.show()
    sys.exit(0)

# default example
source_dir = os.path.abspath(os.path.dirname(__file__))
image_file = os.path.join(source_dir, os.pardir, 'pyautogui/im1.png')
input_im = Image.open(image_file)
bounds = input_im.getbbox()
print('bounds:', bounds)
left, upper, right, lower = bounds

menu_box = (left, upper, right, 100)
menu_im = input_im.crop(menu_box)

# Timeout/terminate the tesseract job after a period of time
# try:
#     print(pytesseract.image_to_string('test.jpg', timeout=2)) # Timeout after 2 seconds
#     print(pytesseract.image_to_string('test.jpg', timeout=0.5)) # Timeout after half a second
# except RuntimeError as timeout_error:
#     # Tesseract processing is terminated
#     pass

# image_to_string has no box coordinates
# print(pytesseract.image_to_string(menu_im))

# image_to_boxes doesn't group strings
# print(pytesseract.image_to_boxes(menu_im))

# Get verbose data including boxes, confidences, line and page numbers
print(pytesseract.image_to_data(menu_im))

# Get HOCR output
# hocr = pytesseract.image_to_pdf_or_hocr('test.png', extension='hocr')
# print(hocr)

# Get ALTO XML output
# xml = pytesseract.image_to_alto_xml(menu_im)
# print('xml:')
# print(xml)

menu_im.show()
