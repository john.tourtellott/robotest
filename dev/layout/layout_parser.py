""""""

import json
import os
import sys

from PIL import Image, ImageDraw
import pytesseract
from tabulate import tabulate
import yaml


# I'll never get modules, but this works...
if __name__ == '__main__':
    from window import Window
else:
    from .window import Window

class LayoutType:
    HBox = 'hbox'
    VBox = 'vbox'


class LayoutParser:
    def __init__(self):
        self.window_dict = dict()  # <name, window>

    def parse(self, spec):
        """Reads window specification(s).

        @input spec: either dictionary or list of dictionaries
        @return list of Window instances
        """
        ret_list = list()
        if isinstance(spec, list):
            for window_spec in spec:
                window = self._parse_root_spec(window_spec)
                ret_list .append(window)
        elif isinstance(spec, dict):
            window = self._parse_root_spec(spec)
            ret_list.append(window)

        return ret_list

    def _parse_root_spec(self, spec):
        """Parses spec for top-level window."""
        name = spec.get('name', 'unnamed')
        position = spec.get('position', [0, 0])
        size = spec.get('size')

        window = Window(name, position=position, size=size)
        self.window_dict[name] = window
        self._parse_window(window, spec)
        return window

    def _check_name(self, name):
        """"""
        if name not in self.window_dict:
            return name

        for i in range(1, 999):
            test_name = '{}-{}'.format(name, i)
            if test_name not in self.window_dict:
                return test_name

    def _parse_hbox(self, parent_window, parent_spec):
        """"""
        x, y = parent_window.position
        parent_width, parent_height = parent_window.size
        parent_window.layout = LayoutType.HBox
        parent_window.children = list()
        last = len(parent_spec) - 1
        remaining_width = parent_width
        for i, spec in enumerate(parent_spec):
            name = spec.get('name', 'window')
            if i == last:
                width = remaining_width
            else:
                width = spec.get('width')
            assert width is not None, 'Spec for window {} missing width'.format(name)
            size = [width, parent_height]

            name = self._check_name(name)
            window = Window(name, [x, y], size)
            self.window_dict[name] = window
            self._parse_window(window, spec)
            parent_window.children.append(window)
            x += width
            remaining_width -= width

    def _parse_vbox(self, parent_window, parent_spec):
        """"""
        x, y = parent_window.position
        parent_width, parent_height = parent_window.size
        parent_window.layout = LayoutType.VBox
        parent_window.children = list()
        last = len(parent_spec) - 1
        remaining_height = parent_height
        for i, spec in enumerate(parent_spec):
            name = spec.get('name', 'window')
            if i == last:
                height = remaining_height
            else:
                height = spec.get('height')
            assert height is not None, 'Spec for window {} missing height'.format(name)
            size = [parent_width, height]

            name = self._check_name(name)
            window = Window(name, [x, y], size)
            self.window_dict[name] = window
            self._parse_window(window, spec)
            parent_window.children.append(window)
            y += height
            remaining_height -= height

    def _parse_window(self, window, spec):
        """"""
        if LayoutType.VBox in spec:
            self._parse_vbox(window, spec.get(LayoutType.VBox))
        elif LayoutType.HBox in spec:
            self._parse_hbox(window, spec.get(LayoutType.HBox))


if __name__ == '__main__':
    # Hard code to the spec file
    source_dir = os.path.abspath(os.path.dirname(__file__))
    spec_filename = 'modelbuilder.yml'
    spec_path = os.path.join(source_dir, spec_filename)
    spec = None
    with open(spec_path) as instream:
        spec = yaml.safe_load(instream)
    # print('data:', spec)

    parser = LayoutParser()
    window_list = parser.parse(spec)
    root_window = window_list[0]
    print('windows:', root_window)
    # print('root_window', root_window)

    d = root_window.to_dict()
    # print(d)
    # j = json.dumps(d, indent=2, sort_keys=True)
    j = json.dumps(d, sort_keys=True)
    print(j)

    print()
    print(parser.window_dict.keys())

    # Use hard-coded path to image
    image_path = os.path.join(source_dir, os.pardir, 'pyautogui/im1.png')
    with Image.open(image_path) as im:
        # im.show()

        draw = ImageDraw.Draw(im)
        offset = 4
        # for name in ['root', 'main_menu', 'main_toolbars', 'main_area', 'left_sidebar', 'central_area', 'renderview']:
        # for name in ['output_messages']:
        for name, window in parser.window_dict.items():
            window = parser.window_dict.get(name)
            print(name, window.position, window.size)
            x0 = window.position[0] + offset
            y0 = window.position[1] + offset
            x1 = x0 + window.size[0] - 2*offset
            y1 = y0 + window.size[1] - 2*offset
            coords = (x0, y0, x1, y1)
            draw.rectangle(coords, outline=(250, 0, 0), width = 2)

        im.show()

    menu_window = parser.window_dict.get('main_menu')
    assert menu_window is not None, 'No main_menu window.'
    menu_im = im.crop(menu_window.bounds())
    # menu_im.show()
    data = pytesseract.image_to_data(menu_im)
    # print(data)
    rows = data.split('\n')
    # print(rows[3])
    table = list()
    table.append(rows[0].split())
    for row in rows[1:]:
        cols = row.split()
        if len(cols) < 12:
            continue
        # print(cols, len(cols))
        table.append(cols)
    print(tabulate(table))

    labels = dict()
    for row in rows[1:]:
        cols = row.split()
        if len(cols) >= 12:
            label = cols[-1]
            xywh = cols[-6:-2]
            labels[label.lower()] = tuple(xywh)
    print(labels)
