"""Try finding horizontal edges in image showing window title bar.

Started from https://stackoverflow.com/a/14140796
"""

import math
import os

import numpy as np
import cv2
from PIL import Image

source_dir = os.path.abspath(os.path.dirname(__file__))
img_path = os.path.join(source_dir, os.pardir, os.pardir, 'locate.png')
img = cv2.imread(img_path)
print('img', img)

# TODO load color image and convert to gray
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
gray = img
edges = cv2.Canny(gray, 80, 120)
lines = cv2.HoughLinesP(edges, 1, math.pi/2, 2, None, 30, 1)
print('lines', lines.shape)
print()

# Reshape to 2D reduced
reduced = np.reshape(lines, (13,4))

# Sort by y1 coordinate
sorted = reduced[np.argsort(reduced[:,1])]
# Draw lines for each edge
# for i in range(2):
#     line = sorted[i]
#     print(line)
#     pt1 = (line[0],line[1])
#     pt2 = (line[2],line[3])
#     cv2.line(img, pt1, pt2, (0,0,255), 3)

# TODO Check that first 2 edges are full-length and horizontal
# TODO Check height between edges 1 and 2

# Fill rectangle below second edge
y_lower = sorted[1][3]
pt1 = (0, y_lower)
pt2 = (img.shape[1], img.shape[0])
print('img.shape', img.shape)
cv2.rectangle(img, pt1, pt2, (128, 128, 128), -1)

filename = 'edges.png'
cv2.imwrite(filename, img)
print()
print(sorted)
print('Wrote', filename)

# Convert to PIL image
img_pil = Image.fromarray(img)
