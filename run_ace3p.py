"""Example/demo for modelbuilder-ace3p.

Uses pyautogui for interaction
Uses Pillow for image capture
"""

import os
import shutil
import sys

import dotenv
import yaml

from PIL import Image, ImageDraw, ImageGrab
import pyautogui
print('pyautogui.size():', pyautogui.size())

import robotest
from robotest import RegionType

PROJECT_FOLDER = os.path.expanduser('~/modelbuilder/projects/project1')
MODEL_FILE = os.path.expanduser('~/projects/ace3p/data/pillbox-rtop4.gen')


if __name__ == '__main__':
    dotenv.load_dotenv()    # print ('RESULT', result)
    debug_mode = os.environ.get('DEBUG', False)

    robo = robotest.Robotest(debug_mode)

    # Read window layout spec
    source_dir = os.path.abspath(os.path.dirname(__file__))
    spec_file = os.path.join(source_dir, 'demo/ace3p.yml')
    assert spec_file is not None, 'No LAYOUT_SPEC in .env file'

    spec_path = os.path.join(source_dir, spec_file)
    spec = None
    with open(spec_path) as instream:
        spec = yaml.safe_load(instream)
    assert spec is not None, 'Failed to open spec file {}'.format(spec_path)
    # print('data:', spec)
    robo.load_layout(spec)

    # Get main_window instance
    main_window = robo.get_window('modelbuilder')
    assert main_window is not None, 'modelbuilder window not found'

    # Make sure project directory is not there
    if os.path.exists(PROJECT_FOLDER):
        print('Removing project folder', PROJECT_FOLDER)
        shutil.rmtree(PROJECT_FOLDER)

    # Start the executable
    exe_path = os.environ.get('MODELBUILDER')
    assert exe_path is not None, 'No MODELBUILDER in .env file'
    assert os.path.exists(exe_path), 'modelbuilder exe not found {}'.format(exe_path)
    mb_proc = robo.start_exe([exe_path, '--dr'], main_window)

    if debug_mode:
        # Grab screenshot and draw rectangles
        im1 = ImageGrab.grab(bbox=(main_window.screen_bbox()))

        # Draw rectangles for each window
        draw = ImageDraw.Draw(im1)
        for name in main_window.region_names():
            region = main_window.region(name)
            if region.type() not in set([ RegionType.Window, RegionType.Region ]):
                print('Region', region.name, region.type())
                continue
            rect = region.geometry()
            print(name, region.position, region.size, rect)
            offset = 0
            x0 = rect[0] + offset
            y0 = rect[1] + offset
            x1 = rect[2] - 2*offset
            y1 = rect[3] - 2*offset
            coords = (x0, y0, x1, y1)
            draw.rectangle(coords, outline=(250, 0, 0), width = 2)

        # Save modelbuilder image with region rectangles
        print('im1', im1)
        print('im1:', im1.format, im1.size, im1.mode)
        path = os.path.join(source_dir, 'im1.png')
        im1.save(path)
        print('Wrote', path)

    # Find main menus
    menubar_region = main_window.region('main_menu')
    robo.locate_fields(menubar_region)

    # Locate menu items
    for menu_region in menubar_region.children:
        robo.locate_menus(menu_region)

    # Invoke menu item to create project
    ace3p_menu = menubar_region.region('ACE3P')
    assert ace3p_menu is not None, \
        'ACE3P_MENU NOT FOUND in menubar {}'.format(menubar_region.region_lookup)

    new_menu = ace3p_menu.region('New')
    assert new_menu is not None, 'NEW PROJECT MENU NOT FOUND'

    # Open ACE3P menu item
    x, y = ace3p_menu.screen_point()
    pyautogui.moveTo(x, y, 0.5)
    pyautogui.mouseDown()

    # Go to "New" item and click
    x, y = new_menu.screen_point()
    pyautogui.moveTo(x, y, 0.5)
    pyautogui.click()
    pyautogui.moveTo(x+10, y, 0.5)

    wizard = robo.get_window('new_project')
    rect = robo.locate_popup_window()
    if debug_mode:
        x, y, w, h = rect
        bbox = (x, y, x + w, y + h)
        wizard_img = ImageGrab.grab(bbox=bbox)
        filename = 'wizard.png'
        wizard_img.save(filename)
        print('WROTE', filename)
    wizard.set_screen_position(rect[:2])

    # Wizard page 1
    # Locate the "Next" button and click it
    buttons_region = wizard.region('buttons')
    next_button = robo.locate_text_field(buttons_region, 'Next')
    x, y = next_button.screen_point()
    pyautogui.moveTo(x, y, 0.5)
    pyautogui.click()
    pyautogui.moveTo(x+10, y, 0.5)

    # Wizard page 2
    # Set Genesis (model file) field
    body_region = wizard.region('body')
    genesis_field = robo.locate_text_field(body_region, 'Genesis')
    screen_coords = genesis_field.screen_point(include_action_offset = True)
    x, y = screen_coords
    pyautogui.moveTo(x, y, 0.5)
    pyautogui.click()
    pyautogui.write(MODEL_FILE)
    pyautogui.moveTo(x+10, y, 0.5)

    # Click the Create Project button
    create_button = robo.locate_text_field(buttons_region, 'Create', threshold=128)
    x, y = create_button.screen_point()
    pyautogui.moveTo(x, y, 0.5)
    pyautogui.click()
    pyautogui.moveTo(x+10, y, 0.5)

    # Wizard page 3
    # Click the Close wizard button
    create_button = robo.locate_text_field(buttons_region, 'Close', threshold=128)
    x, y = create_button.screen_point()
    pyautogui.moveTo(x, y, 0.5)
    pyautogui.click()
    pyautogui.moveTo(x+10, y, 0.5)

    # Click on Analysis selector
    main_region = main_window.region('main_area')
    left_sidebar = main_region.region('left_sidebar')
    analysis_field = robo.locate_text_field(left_sidebar, '[Analysis')
    screen_coords = analysis_field.screen_point(include_action_offset = True)
    x, y = screen_coords
    pyautogui.moveTo(x, y, 0.5)
    pyautogui.click()
    pyautogui.moveTo(x+10, y, 0.5)

    # Wait for modelbuilder to close
    print('Waiting for user to close modelbuilder')
    mb_proc.wait()
